console.log('Start')

const cob = function(mas) {
  console.log(mas);
};

let mas = ["Джас","Блюз"];
cob(mas); // 1

mas.push("Рок-н-ролл");
cob(mas); // 2

mas[Math.floor((mas.length - 1) / 2)] = "Классика";
cob(mas); // 3 Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.

mas.shift();
cob(mas); // 4

mas.unshift("Рэп", "Регги");
cob(mas); // 5

console.log('End')
