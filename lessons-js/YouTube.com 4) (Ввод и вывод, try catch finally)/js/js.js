function ab() {
  let a = parseInt(document.getElementById('in_a').value);
  let b = parseInt(document.getElementById('in_b').value);

  console.log(a);
  console.log(b);

  alert(a+b);
}

let i=0;

try {
  i = 10;
  throw new Error("error 1");
} catch (ex) {
  i = i + 100
  console.error("Error: " + ex.message);
} finally {
  i = i + 1000
  console.log("block finally")
}

console.log("i = "+i);
