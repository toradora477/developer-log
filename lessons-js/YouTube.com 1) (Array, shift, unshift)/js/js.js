let a_1 = ["1","2",'3','4','5','6','7','8','9','10'];
console.log('a_1', a_1, a_1.length);

let a_2 = a_1;
console.log('a_2', a_2, a_2.length);

console.log('////////////');

let d_1 = a_1.shift();
console.log('d_1', d_1, d_1.length);
console.log('a_1', a_1, a_1.length);

let d_2 = a_1.unshift(100, 200, 300);
console.log('d_2', d_2, d_2.length);
console.log('a_1', a_1, a_1.length);

console.log('////////////');

let c_1 = a_1.pop();
console.log('c_1', c_1, c_1.length);
console.log('a_1', a_1, a_1.length);

let c_2 = a_1.push('Red', 'Light', 'Dark');
console.log('c_2', c_2, c_2.length);
console.log('a_1', a_1, a_1.length);

console.log('////////////');

let f_2 = a_1.slice(3,7);
console.log('f_2', f_2, f_2.length);
console.log('a_1', a_1, a_1.length);

let f_1 = f_2.concat(a_1,[18,'Hello']);
console.log('f_1', f_1, f_1.length);
console.log('a_1', a_1, a_1.length);

console.log('////////////');

let v_2 = a_1.splice(3,7,"splice",9999,8888);
console.log('v_2', v_2, v_2.length);
console.log('a_1', a_1, a_1.length);
