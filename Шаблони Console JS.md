# Сommands for console in JavaScript

_/_ Основные команды _/_
console.log()                        \_\_ Вывод данных в консоль
console.info()                       \_\_ Вывод информационного сообщения
console.error()                      \_\_ Вывод сообщения об ошибке
console.warn()                       \_\_ Вывод предупреждения

_/_ Второстепенные команды _/_
console.debug()                      \_\_ Вывод сообщения для отладки (аналог log)
console.dir()                        \_\_ Отображение свойств объекта в виде дерева
console.clear()                      \_\_ Очистка консоли
console.table()                      \_\_ Вывод данных в табличном виде
console.count()                      \_\_ Счетчик вызовов с уникальной меткой
console.countReset()                 \_\_ Сброс счетчика с указанной меткой
console.assert(condition, message)   \_\_ Вывод сообщения об ошибке, если условие ложно
console.trace()                      \_\_ Вывод трассировки стека вызовов

_/_ Время и группы _/_
console.time('label')                \_\_ Начало измерения времени для блока кода с меткой
console.timeLog('label')             \_\_ Вывод промежуточного времени с меткой
console.timeEnd('label')             \_\_ Завершение измерения времени и вывод результата

_/_ Группировка сообщений _/_
console.group('label')               \_\_ Создание группы сообщений с отступом
console.groupCollapsed('label')      \_\_ Создание свернутой группы сообщений
console.groupEnd('label')            \_\_ Закрытие группы сообщений
